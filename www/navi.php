<form action="#" method="get">
    <div>
        Anfangsdatum:<br>
        <input type="text" name="vondatumstr" id="i_vondatum" style="width: 120px; height: 35px;" readonly value="<?php echo $_GET["vondatumstr"] ?? "";?>"/>
        <button type="reset" id="vondatum_trigger" style="float: right;	width: 30px; height: 35px;">...</button>
    </div>
	<br>
    <div>
        Enddatum:<br>
        <input type="text" name="bisdatumstr" id="i_bisdatum" style="width: 120px; height: 35px;" readonly value="<?php echo $_GET["bisdatumstr"] ?? "";?>"/>
        <button type="reset" id="bisdatum_trigger" style="float: right; width: 30px; height: 35px;">...</button>
    </div>
    <br>
    <div>
		<fieldset>
			<legend>Darstellung</legend>
			<input type="radio" id="radio1" name="auswahl" value="insgesamt" 
				<?php if((isset($_GET["auswahl"]) &&  $_GET["auswahl"] == "insgesamt") || 
				(!(isset($_GET["auswahl"]) &&  $_GET["auswahl"] == "taeglich") &&
				!(isset($_GET["auswahl"]) &&  $_GET["auswahl"] == "graphisch")))
				echo "checked"?>>
			<label for="radio1"><span></span>Insgesamt</label>
			<br>
			<input type="radio" id="radio2" name="auswahl" value="taeglich" 
				<?php if(isset($_GET["auswahl"]) &&  $_GET["auswahl"] == "taeglich") echo "checked"?>>
			<label for="radio2"><span></span>Tägliches Mittel</label>
			<br>
			<input type="radio" id="radio3" name="auswahl" value="graphisch" 
				<?php if(isset($_GET["auswahl"]) &&  $_GET["auswahl"] == "graphisch") echo "checked"?>>
			<label for="radio3"><span></span>Graphisch</label>
		</fieldet>
    </div>
    <br>
    <div>
        <button type="submit" style="height: 35px; width: 100%;">Absenden</button>
    </div>
</form>
<script type="text/javascript">
	function catcalc(cal){
		if(document.getElementById("i_bisdatum").value == 0)
			document.getElementById("i_bisdatum").value = document.getElementById("i_vondatum").value;
		date_a = document.getElementById("i_vondatum").value;
		date_b = document.getElementById("i_bisdatum").value;
        date_a_stamp = Date.parse(date_a);
        date_b_stamp = Date.parse(date_b);
        if(date_b_stamp < date_a_stamp)
            document.getElementById("i_bisdatum").value = document.getElementById("i_vondatum").value;
	}
			
    Calendar.setup({
        inputField     :    "i_vondatum",      // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "vondatum_trigger",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,                // show all years in drop-down boxes 
	onUpdate       : catcalc
    });
    Calendar.setup({
        inputField     :    "i_bisdatum",      // id of the input field
        ifFormat       :    "%Y-%m-%d",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "bisdatum_trigger",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1,                // show all years in drop-down boxes 
	onUpdate       : catcalc
    });
</script>
