<?php
require_once("readdb.php");
echo ("<table id=\"meitab\">");
echo ("<thead><tr>");
echo ("<th id=\"meicell\">MAC</th><th id=\"meicell\">IN</th><th id=\"meicell\">OUT</th>");
echo ("</tr></thead>");
echo ("<tbody>");
foreach ($macs as $mac){
	$intrafz = 0;
	$outtrafz = 0;
	echo("<tr>");
	foreach ($data as $val)
		if($val["mac"] == $mac){
			$intrafz += $val["intraf"];
			$outtrafz += $val["outtraf"];
		}
	printf("<td id=\"meicell\">%s</td><td id=\"meicell\">%s</td><td id=\"meicell\">%s</td>", $mac, 
		shorten($intrafz), shorten($outtrafz));
	echo ("</tr>");
}	
echo ("</tbody>");
echo ("</table>");
if($bdata)
	echo("<br><br>Es liegen nicht für den gesamten Zeitraum Daten vor!<br><br>");

function shorten($a) {
	$kuerzel = "b";
	if($a >= 1024){
		$a = round($a / 1024);
		$kuerzel = "kb";
	}
	if($a >= 1024){
		$a = round($a / 1024);
		$kuerzel = "mb";
	}
	if($a >= 1024){
		$a = round($a / 1024);
		$kuerzel = "gb";
	}
	return(strval($a)." ".$kuerzel);
}

?>
