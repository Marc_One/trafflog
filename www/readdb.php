<?php

#if(!$vondatum || !$bisdatum){
#	$vondatum = array("tag" => 8, #intval(date("j")),
#			"monat" => 12, #intval(date("n")),
#			"jahr" => 2017, #intval(date("Y"))
#			);
#	$bisdatum = $vondatum;
#}

sscanf($_GET["vondatumstr"], "%u-%u-%u", $vondatum["jahr"], $vondatum["monat"], $vondatum["tag"]);
sscanf($_GET["bisdatumstr"], "%u-%u-%u", $bisdatum["jahr"], $bisdatum["monat"], $bisdatum["tag"]);

#printf("%u-%02u-%02u", $vondatum["jahr"], $vondatum["monat"], $vondatum["tag"]);
$jahre = $bisdatum["jahr"] - $vondatum["jahr"];
if($jahre == 0)
	for($m = $vondatum["monat"]; $m <= $bisdatum["monat"]; $m++)
		$monate[] = sprintf("%u-%02u", $vondatum["jahr"], $m);
else{
	for($m = $vondatum["monat"]; $m <= 12; $m++)
		$monate[] = sprintf("%u-%02u", $vondatum["jahr"], $m);
	for($m = 1; $m <= $bisdatum["monat"]; $m++)
		$monate[] = sprintf("%u-%02u", $bisdatum["jahr"], $m);
	if($jahre > 1)
		for($i = 1; $i < $jahre; $i++)
			for($j = 1; $j<=12; $j++)
				$monate[] = sprintf("%u-%02u", $vondatum["jahr"] + $i, $j);
}

$bdata = 0;
foreach ($monate as $monat){
	if(!file_exists("/var/local/trlog/trlog-" . $monat . ".db")){
		$bdata = 1;
		unset($monate[array_search($monat, $monate)]);
	}
}
$monate = array_values($monate);
#var_dump($monate);

$db = new SQLite3(":memory:");
$db->exec("CREATE TABLE IF NOT EXISTS traffic (id INTEGER PRIMARY KEY AUTOINCREMENT, ".
	"mac TEXT, time INTEGER, intraf INTEGER, outtraf INTEGER);");
foreach ($monate as $monat){
	#printf("%s<br>", $monat);
	$z = 0;
	while(!@$db->exec("ATTACH DATABASE '/var/local/trlog/trlog-" . $monat . ".db' AS source;") && ($z < 10)){
		sleep(1);
		$z++;
	}
	if($z == 10)
		exit("Fehler beim Öffnen der Datenbank!");
	$db->exec("INSERT INTO traffic SELECT NULL, mac, time, ".
		"intraf, outtraf FROM source.traffic;");
	$db->exec("DETACH DATABASE source;");
}
$results = $db->query("SELECT * FROM traffic WHERE time BETWEEN '".$vondatum["jahr"]."-".
			sprintf("%02u", $vondatum["monat"])."-".sprintf("%02u", $vondatum["tag"]).
			" 00:00' AND '".$bisdatum["jahr"]."-".
			sprintf("%02u", $bisdatum["monat"])."-".sprintf("%02u", $bisdatum["tag"])." 23:59';");

$data = array();
$macs = array();
while($row = $results->fetchArray()){
	$data[] = array("mac" => $row["mac"], "time" => $row["time"], 
		"intraf" => $row["intraf"], "outtraf" => $row["outtraf"]);
	if(!in_array($row["mac"], $macs))
		$macs[] = $row["mac"];
}

$db->close();

#foreach($data as $dat)
#	$outtemp += $dat["outtraf"];
#echo($outtemp."\n");
#var_dump($data);

?>

