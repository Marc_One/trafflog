<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();
?>

<html>
<head>
	<title>Traffic Logger</title>
	<link rel="stylesheet" type="text/css" media="all" href="cal/calendar-win2k-cold-1.css" title="win2k-cold-1" />
	<script type="text/javascript" src="cal/calendar.js"></script>
	<script type="text/javascript" src="cal/lang/calendar-de2.js"></script>
	<script type="text/javascript" src="cal/calendar-setup.js"></script>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>

<div id="index">
    <div id="ueber">
    Traffic Logger
    </div>
    <div id="navi">
    <?php require_once("navi.php"); ?>
    </div>
    <div id="traf">
    <?php require_once("traf.php"); ?>
    </div>
</div>
</body>
</html>
