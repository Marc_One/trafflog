<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css">
        <title>Traffic Logger</title>
    </head>
    <body>
        <div class="container">
            <div class="col-8">
                sample text...
            </div>
        </div>
        <h1>Hello, world!</h1>
        <script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
        <script src="./bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
