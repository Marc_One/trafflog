<form action="#" method="get">
    <div id="datum">
        Zeitrahmen<br>
        <input name="datumstr" id="date_selector" readonly style="font-size:10px"/><br>
        <button type="submit" id="blabutton">Absenden</button>
    </div>
</form>
<script type="text/javascript">
    const fp = flatpickr("#date_selector", {"locale": "de"});
    fp.set("mode","range");
</script>
