<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();
?>

<html>
<head>
	<title>Traffic Logger</title>
	<link rel="stylesheet" type="text/css" href="flatpickr.min.css"/>
	<script type="text/javascript" src="flatpickr.js"></script>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<script type="text/javascript" src="de.js"></script>
</head>
<body>

<div id="seite">
    <div id="ueber">
        Traffic Logger
    </div>
    <div id="navi">
        <?php require_once("navi.php"); ?>
    </div>
    <div id="traf">
        <?php require_once("traf.php"); ?>
    </div>
</div>
</body>
</html>
