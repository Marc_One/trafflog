<?php
//if($_GET["vondatumstr"] == $_GET["bisdatumstr"])
//	exit("Zeitraum muss mindestens zwei Tage betragen!");
require_once("readdb.php");

if(!count($data))
	exit("Keine Daten zum darstellen vorhanden!");

$perm = umask();
umask(0113);

$uniqid = uniqid();
$data_file = "/tmp/gnuplotout" . $uniqid;
$image_file = "/tmp/gnuplotout" . $uniqid . ".jpg";

$alletage = array();
foreach($data as $val)
	if(!in_array(substr($val["time"], 0, 10), $alletage))
		$alletage[] = substr($val["time"], 0, 10);

$file = fopen($data_file, "w+");
foreach($macs as $mac){
	fwrite($file, $mac . "\n");
	foreach($alletage as $diesertag){
		$intraf = $outtraf = 0;
		foreach($data as $val){
			if(substr($val["time"], 0, 10) == $diesertag && $val["mac"] == $mac){
					$intraf += $val["intraf"];
					$outtraf += $val["outtraf"];
			}
		}
		fwrite($file, $diesertag . "|" . ($intraf + $outtraf) . "\n");
	}
	fwrite($file, "\n\n");
}
fclose($file);

$nummacs = count($macs);

$rangevon = eintagweniger($vondatum["jahr"], $vondatum["monat"], $vondatum["tag"]);
$rangebis = eintagmehr($bisdatum["jahr"], $bisdatum["monat"], $bisdatum["tag"]);
#print $rangevon;
#print $rangebis;

$gnuplot_cmds = <<< GNUPLOTCMDS
set term jpeg
set output "$image_file"
set xdata time
set timefmt "%Y-%m-%d"
set xrange ["$rangevon":"$rangebis"]
set datafile separator "|"
set key opaque
set key box
set format x "%d.%m\\n%Y"
set xtics font ",10"
plot for [IDX=0:$nummacs] "$data_file" i IDX using 1:2 with boxes fs solid title columnheader
quit
GNUPLOTCMDS;
#$gnuplot_cmds .= "\n";

// Start gnuplot
if(!($pgp = popen("/usr/bin/gnuplot", "w")))
    exit("Gnuplot konnte nicht ausgeführt werden!");

fputs($pgp, $gnuplot_cmds);
pclose($pgp);

// Clean up and exit
unlink($data_file);
$_SESSION["image_file"] = $image_file;
echo "<img src=\"image.php\" width=\"398\"></img>";
//require_once("./image.php");
#unlink($image_file);
umask($perm);

function eintagweniger($jahr, $monat, $tag){
	$datum = strtotime(sprintf("%u-%02u-%02u", $jahr, $monat, $tag));
	$datum -= 86400;
	return(date("Y-m-d", $datum));
/*
	$tag--;
	if($tag < 1){
		$tag = 1;
		$monat--;
	}
	if($monat < 1){
		$monat = 12;
		$jahr--;
	}
	return(sprintf("%u-%02u-%02u", $jahr, $monat, $tag));*/
}

function eintagmehr($jahr, $monat, $tag){
	$datum = strtotime(sprintf("%u-%02u-%02u", $jahr, $monat, $tag));
	$datum += 86400;
	return(date("Y-m-d", $datum));/*
	$tag++;
	if($tag > 31){
		$tag = 31;
		$monat++;
	}
	if($monat > 12){
		$monat = 1;
		$jahr++;
	}
	return(sprintf("%u-%02u-%02u", $jahr, $monat, $tag));*/
}
?>
