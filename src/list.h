#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

struct _tmac{
	unsigned char a,b,c,d,e,f;
};
typedef struct _tmac tmac;

struct _trafficlist{
	struct _tmac mac;
	long intraffic;
	long outtraffic;
	struct _trafficlist* next;
};
typedef struct _trafficlist trafficlist;

void insert(trafficlist** liste, tmac mac, long intarf, long outtraf);
void testlist(trafficlist* testobj);
bool macvergleich(tmac in1, tmac in2);
void freelist(trafficlist* liste);
void printmac(tmac* mac);
void mactochar(tmac* mac, char* out);

#endif
