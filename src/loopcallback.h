#ifndef LOOPCALLBACK_H
#define LOOPCALLBACK_H

#include "list.h"
#include <pcap.h>
#define ETHERNET_HEADER_LEN 14

struct _argtype{
	trafficlist* list;
	tmac mac;
};
typedef struct _argtype argtype;

void loopcallback(unsigned char* args, const struct pcap_pkthdr* header, const unsigned char* packet);
bool macreserved(tmac mac);

#endif
