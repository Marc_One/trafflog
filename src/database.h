#ifndef DATABASE_H
#define DATABASE_H

#include "list.h"

#define MAXNAME 256
#define DBPATH /var/local/trafflog/

void writetodb(trafficlist* list);

#endif
