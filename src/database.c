#include "database.h"
#include <sqlite3.h>
#include "list.h"
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>

void writetodb(trafficlist* list){
	char filename[MAXNAME];
	char tmp[20];
	sqlite3* db;
	struct tm* timestruct;
	time_t secs;
	
	time(&secs);
	timestruct = localtime(&secs);
	strftime(tmp, 20, "trlog-%Y-%m.db", timestruct);
	const char* folder = "/var/local/trlog/";
	sprintf(filename, "%s%s", folder, tmp);
	struct stat sb;
	if (!(!stat(folder, &sb) && S_ISDIR(sb.st_mode)))
		mkdir(folder, 0755);
	if(sqlite3_open(filename, &db)){
		printf("Fehler beim Öffnen der Datenbank.\n");
		return;
		}

	char* errmsg;
	sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS traffic (id INTEGER PRIMARY KEY AUTOINCREMENT,"
		 " mac TEXT, time INTEGER, intraf INTEGER, outtraf INTEGER);", NULL, NULL, &errmsg);
	if(errmsg){
		printf("Sqlite3 error: %s\n",errmsg);
		sqlite3_free(errmsg);
		sqlite3_close(db);
		return;
		}
	while(list != NULL){
		char sqlstring[256];
		char macstr[18];
		mactochar(&list->mac, macstr);
		sprintf(sqlstring, "INSERT INTO traffic VALUES (NULL, \'%s\', datetime(),\
			 %d, %d);", macstr, list->intraffic, list->outtraffic);
		sqlite3_exec(db, sqlstring, NULL, NULL, &errmsg);
		if(errmsg){
			printf("Sqlite3 error: %s\n",errmsg);
			sqlite3_free(errmsg);
			sqlite3_close(db);
			return;
			}
		list = list->next;
		}
	
	sqlite3_close(db);
}
