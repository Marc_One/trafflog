#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include "list.h"

struct _configuration{
	tmac mac;
	char device[20];
	int interval;
};
typedef struct _configuration configuration;
	
//int main(int argc, char* argv[]);
int readconfig(configuration* confout, char* filelocation);
//bool checkformac(char* arg);

#endif
