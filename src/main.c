#include <stdio.h>
#include <stdlib.h>
#include "main.h"
//#include "init.h"
#include "loopcallback.h"
#include "list.h"
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include "database.h"
#include <stdbool.h>
#include <ctype.h>
#include <pcap.h>
#include <time.h>
#include "configfile.h"

pcap_t* glob_devhandle;
bool bquit = false;

int main(int argc, char* argv[]){
	int c;
	tmac mac;
	char* devicename = NULL;
	int interval = 0;
	bool bmacgiven = false;
	bool bfreedevicename = false;
	char errbuf[PCAP_ERRBUF_SIZE];
	int rcerr;
	configuration config;
	if(argc < 2){
		if((rcerr = readconfig(&config, NULL))){
			exit(78);
		}
		devicename = strdup(config.device);
		memcpy(&mac, &config.mac, sizeof(mac));
		interval = config.interval;
	}
	else{
		while ((c = getopt(argc, argv, "m:d:i:")) != -1){
			switch (c){
				case 'm':
					if(!checkformac(optarg)){
						printf("Wrong MAC adress format!\n");
						exit(8);
					}
					sscanf(optarg, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx", \
						&mac.a, &mac.b, &mac.c, &mac.d, &mac.e, &mac.f);
					bmacgiven = true;
					break;
				case 'd':
					devicename = strdup(optarg);			
					break;
				case 'i':
					interval = atoi(optarg);
					if(!interval){
						printf("Interval value not valid!\n");
						exit(9);
					}
					break;
				default:
					help();
					
			}
		}
	}
	if(!interval)
		interval = 5;
	if(!bmacgiven){
		help();
		free(devicename);
		exit(33);
	}
	if(!devicename){
		devicename = pcap_lookupdev(errbuf);
		if(devicename == NULL){
			printf("pcap_lookupdev(): %s\n", errbuf);
			exit(6);
		}
	}

	pcap_t* devhandle;
	devhandle = pcap_open_live(devicename, BUFSIZ, 1, 0, errbuf);
	if(devhandle == NULL){
		printf("pcap_open_live(): %s\n", errbuf);
		exit(1);
	}
	glob_devhandle = devhandle;

	signal( SIGINT, (__sighandler_t) quit_handler);
	signal( SIGALRM, (__sighandler_t) alarm_handler);
	
	argtype callbackargs;
	callbackargs.list = NULL;
	callbackargs.mac = mac;	

	printf("Counting traffic for MAC: ");
	printmac(&mac);
	printf(".\nOn device: %s. \nWriting to database every %d seconds.\n", devicename, interval);	
	while(!bquit){
		alarm(interval);
		pcap_loop(devhandle, -1, loopcallback, (unsigned char*) &callbackargs);
		testlist(callbackargs.list);
		writetodb(callbackargs.list);
		freelist(callbackargs.list);
		callbackargs.list = NULL;
	}
	pcap_close(devhandle);
	free(devicename);
}

void quit_handler(sig_t s){
	pcap_breakloop(glob_devhandle);
	bquit = true;
}

void alarm_handler(sig_t s){
	pcap_breakloop(glob_devhandle);
}

bool checkformac(char* arg){
	if(strlen(arg) != 17)
		return false;
	for(int i=0; i<17;i++){
		if(i % 3 !=2 && !isxdigit(arg[i]))
			return false;
		if(i % 3 == 2 && arg[i] != ':')
			return false;
	}
	if(arg[17] != '\0')
		return false;
	return true;
}

void help(){
	printf("Options:\n-m [MAC] : specify target mac address\n-d [DEVICE] : specify target"\
		" device (default: auto)\n-i [NUMBER] : specify database write interval"\
		" (default: 5 sec)\nYou must at least specify a mac address!\n");
}
