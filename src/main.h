#ifndef MAIN_H
#define MAIN_H

#include <signal.h>
#include <stdbool.h>

//#define INTERVAL_MINS 10

int main(int argc, char* argv[]);
void quit_handler(sig_t s);
void alarm_handler(sig_t s);
bool checkformac(char* arg);
void help();

#endif
