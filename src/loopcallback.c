#include <pcap.h>
#include "loopcallback.h"
#include "list.h"
#include <string.h>

void loopcallback(unsigned char* args, const struct pcap_pkthdr* header, const unsigned char* packet){
	tmac inmac;
	tmac outmac;
	//tmac target = {0x52,0x54,0x00,0x12,0x35,0x02};
	tmac target  = ((argtype*) args)->mac;
	memcpy(&outmac, packet, 6);
	memcpy(&inmac, packet+6,6);
	if(macvergleich(target, inmac) && !macreserved(outmac))
		insert((trafficlist**)&((argtype*)args)->list, outmac, header->len, 0);
	if(macvergleich(target, outmac) && !macreserved(inmac))
		insert((trafficlist**)&((argtype*)args)->list, inmac, 0, header->len);
}

bool macreserved(tmac mac){
	char broadcast[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	char multicast[] = {0x01,0x00,0x5e,0x00,0x00,0x00};
	char ipv6multicast[] = {0x33,0x33,0x00,0x00,0x00,0x00};
	char vrrp[] = {0x00,0x00,0x5e,0x00,0x01,0x00};

	if(!memcmp(&mac, &broadcast, 6))
		return true;
	if(!memcmp(&mac, &multicast, 3))
		return true;
	if(!memcmp(&mac, &ipv6multicast, 2))
		return true;
	if(!memcmp(&mac, &vrrp, 5))
		return true;
	
	return false;
}
