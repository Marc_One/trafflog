#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool macvergleich(tmac in1, tmac in2){
	if(!memcmp(&in1, &in2, 6))
		return true;
	return false;
} 

void insert(trafficlist** list, tmac mac, long intraf, long outtraf){
	bool bmacexists = false;
	trafficlist* position;
	position = *list;
	if(position !=0){
		if(macvergleich(position->mac, mac)){
			position->intraffic = position->intraffic + intraf;
			position->outtraffic = position->outtraffic + outtraf;			
			bmacexists = true;
		}
		while(position->next != 0){
			position = position->next;
			if(macvergleich(position->mac ,mac)){
				position->intraffic = position->intraffic + intraf;
				position->outtraffic = position->outtraffic + outtraf;	
				bmacexists = true;
			}
		}
	}	
	position = *list;
	if(!bmacexists){
		trafficlist* new = calloc(1, sizeof(trafficlist));
		new->mac = mac;
		new->intraffic = intraf;
		new->outtraffic = outtraf;
		if(*list == NULL){
			*list = new;
		}
		else{	
			while(position->next != NULL){
				position = position->next;
			};
			position->next = new;
		}
	}
}


void testlist(trafficlist* testobj){
	trafficlist* position;
	position = testobj;
	if(testobj == NULL)
		printf("List empty...\n");
	else{
	printf("Mac: ");
	printmac(&position->mac);
	printf(" IN:%d OUT: %d\n", position->intraffic, position->outtraffic);
	while(position->next != NULL){
		position = position->next;
		printf("Mac: ");
		printmac(&position->mac);
		printf(" IN:%d OUT: %d\n", position->intraffic, position->outtraffic);
		}
	}
}

void printmac(tmac* mac){
	for(int i = 0; i < 6; i++){
		if(i<5)
			printf("%02hhX:", *((unsigned char*)mac + i));
		else
			printf("%02hhX", *((unsigned char*)mac + i));
	}
}
	

void freelist(trafficlist* list){
	trafficlist* position = list;
	trafficlist* tmp;
	if(list != NULL)
	while(1){
		tmp = position->next;
		free(position);
		position = tmp;
		if(position == NULL)
			return;
	}
}

void mactochar(tmac* mac, char* out){
	sprintf(out, "%02hhX:%02hhX:%02hhX:%02hhX:%02hhX:%02hhX", (char)mac->a, (char)mac->b, \
		(char)mac->c, (char)mac->d, (char)mac->e, (char)mac->f); 
}

