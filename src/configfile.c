#include "configfile.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "main.h"

//#define _GNU_SOURCE

/*int main(int argc, char* argv[]){
	configuration conf;
	if(!readconfig(&conf)){
		char temp[18];
		sprintf(temp, "%02X:%02X:%02X:%02X:%02X:%02X", conf.mac.a, conf.mac.b, conf.mac.c, conf.mac.d, \
			conf.mac.e, conf.mac.f);
		printf("device: %s mac: %s interval: %u\n", conf.device, temp, conf.interval);
	}
}*/

int readconfig(configuration* confout, char* filelocation){
	memset(confout, 0, sizeof(configuration));


	
	FILE* configfile= fopen("/usr/local/etc/trafflog.conf", "r");
	if(!configfile){
		perror("Cannot open configuration file!\n");
		return(33);
	}
	
	bool berrors = false;
	
	char* buffer = NULL;
	size_t len = 0;
	ssize_t charread;
	char* option;
	while((charread = getline(&buffer, &len, configfile)) != -1){
		if(buffer[0] == '#')
			continue;
		else if(buffer[0] == '\n')
			continue;
		else if(!strncmp(buffer, "device", strlen("device"))){
			option = calloc(1, (charread - strlen("device")));
			sscanf(buffer, "%*s %s", option);
			if(strlen(option) == 0){
				printf("Configuration file: Device name zero length!\n");
				berrors = true;

			}
			if(strlen(option) < 20)
				strcpy(confout->device, option);
			else{
				printf("Configuration file: Device name too long!\n");
				berrors = true;
			}
			free(option);
			free(buffer);
		}
		else if(!strncmp(buffer, "mac", strlen("mac"))){
			option = calloc(1 ,charread - strlen("mac"));
			sscanf(buffer, "%*s %s", option);
			if(checkformac(option))
				sscanf(option, "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx", \
					&confout->mac.a, &confout->mac.b, &confout->mac.c, \
					&confout->mac.d, &confout->mac.e, &confout->mac.f);
			else{
				printf("Configuration file: Mac address not valid!\n");
				berrors = true;
			}
			free(option);
			free(buffer);
		}
		else if(!strncmp(buffer, "interval", strlen("interval"))){
			option = calloc(1, charread - strlen("interval"));
			sscanf(buffer, "%*s %s", option);
			if(atoi(option))
				confout->interval = atoi(option);
			else{
				printf("Configuration file: Interval value wrong!\n");
				berrors = true;
			}
			free(option);
			free(buffer);
		}
		else{
			free(buffer);
			printf("Configuration file: General error!\n");
			berrors = true;
		}
		buffer = NULL;
		len = 0;
	}
	free(buffer);
	fclose(configfile);
	if(((strlen(confout->device)) < 1) || (confout->mac.a + confout->mac.b + confout->mac.c + \
			confout->mac.d + confout->mac.e + confout->mac.f < 1) || (confout->interval < 1)){
		printf("Configuration file: Missing options!\n");
		berrors = true;
	}
	if(berrors)
		return(33);
	else
		return(0);
}

/*bool checkformac(char* arg){
	if(strlen(arg) != 17)
		return false;
	for(int i=0; i<17;i++){
		if(i % 3 !=2 && !isxdigit(arg[i]))
			return false;
		if(i % 3 == 2 && arg[i] != ':')
			return false;
	}
	if(arg[17] != '\0')
		return false;
	return true;
}*/

